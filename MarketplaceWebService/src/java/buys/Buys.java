/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buys;

import account.accountObject;
import item.Items;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author raudi
 */
@WebService(serviceName = "Buys")
public class Buys {

    /**
    * Web service operation
     * @param sellerID
     * @return 
    */
    @WebMethod(operationName = "sales")
    @WebResult(name="buysObject")
    public ArrayList<buysObject> sales (@WebParam(name = "sellerID") int sellerID) {
        ArrayList<buysObject> items = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                    "root",
                    "");
        
            Statement stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM buys WHERE id_seller = ?";
            PreparedStatement dbStatement = conn.prepareStatement(sql);
            dbStatement.setInt(1, sellerID);
            /* Get every data returned by SQL query */
            ResultSet rs = dbStatement.executeQuery();
            /* Get every data returned by SQL query */
            int i = 0;
            while(rs.next()){
                items.add(new buysObject( rs.getInt("id"),
                rs.getInt("id_item"),
                rs.getInt("id_buyer"),
                rs.getInt("id_seller"),
                rs.getString("photo"),
                rs.getString("item_name"),
                rs.getInt("item_price"),
                rs.getInt("quantity"),
                rs.getString("consignee"),
                rs.getString("full_address"),
                rs.getString("postal_code"),
                rs.getString("phone_number"),
                rs.getString("cc_number"),
                rs.getString("verification_code"),
                rs.getString("date"),
                rs.getString("time")
                ));
                ++i;
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }
    
    /**
    * Web service operation
     * @param buyerID
     * @return 
    */
    @WebMethod(operationName = "purchases")
    @WebResult(name="buysObject")
    public ArrayList<buysObject> purchases (@WebParam(name = "buyerID") int buyerID) {
        ArrayList<buysObject> items = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                    "root",
                    "");
        
            Statement stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM buys WHERE id_buyer = ?";
            PreparedStatement dbStatement = conn.prepareStatement(sql);
            dbStatement.setInt(1, buyerID);
            /* Get every data returned by SQL query */
            ResultSet rs = dbStatement.executeQuery();
            /* Get every data returned by SQL query */
            int i = 0;
            while(rs.next()){
                items.add(new buysObject( rs.getInt("id"),
                rs.getInt("id_item"),
                rs.getInt("id_buyer"),
                rs.getInt("id_seller"),
                rs.getString("photo"),
                rs.getString("item_name"),
                rs.getInt("item_price"),
                rs.getInt("quantity"),
                rs.getString("consignee"),
                rs.getString("full_address"),
                rs.getString("postal_code"),
                rs.getString("phone_number"),
                rs.getString("cc_number"),
                rs.getString("verification_code"),
                rs.getString("date"),
                rs.getString("time")
                ));
                ++i;
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }
    
    /**
    * Web service operation
     * @param access_token
     * @param id_item 
     * @param id_buyer 
     * @param id_seller 
     * @param photo 
     * @param item_name 
     * @param item_price 
     * @param time 
     * @param quantity 
     * @param date 
     * @param consignee 
     * @param verification_code 
     * @param full_address 
     * @param cc_number 
     * @param postal_code 
     * @param phone_number 
    */
    @WebMethod(operationName = "confirmPurchase")
    @WebResult(name="buysObject")
    public String confirmPurchase (@WebParam(name = "access_token") String access_token,
                            @WebParam(name = "userAgent") String userAgent,
                            @WebParam(name = "ip") String ip,
                            @WebParam(name = "id_item") int id_item,
                            @WebParam(name = "id_buyer") int id_buyer,
                            @WebParam(name = "id_seller") int id_seller,
                            @WebParam(name = "photo") String photo,
                            @WebParam(name = "item_name") String item_name,
                            @WebParam(name = "item_price") int item_price,
                            @WebParam(name = "quantity") int quantity,
                            @WebParam(name = "consignee") String consignee,
                            @WebParam(name = "full_address") String full_address,
                            @WebParam(name = "postal_code") String postal_code,
                            @WebParam(name = "phone_number") String phone_number,
                            @WebParam(name = "cc_number") String cc_number,
                            @WebParam(name = "verification_code") String verification_code,
                            @WebParam(name = "date") String date,
                            @WebParam(name = "time") String time) {
        
        int result = 0;
        String url = "http://localhost:8001/IdentityService/ValidateToken";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        //con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "token="+access_token+"&browser="+userAgent+"&ip_address="+ip;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer response = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        //System.out.println(response.toString());
        
        accountObject account = null;
        JSONObject JSobjek = null;  
        try {
            //account = new accountObject();
            JSobjek = new JSONObject(response.toString());
            account = new accountObject((int)JSobjek.get("id"), 
                                    (String)JSobjek.get("fullname"),
                                    (String)JSobjek.get("username"),
                                    (String)JSobjek.get("address"),
                                    (String)JSobjek.get("postalcode"),
                                    (String)JSobjek.get("phonenumber"),
                                    (String)JSobjek.get("token"),
                                    (int)JSobjek.get("validate") );
        } catch (JSONException ex) {
            //account = new accountObject();
        }
        try {
            if (account.getValidate() == 1) {
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                        "root",
                        "");

                Statement stmt = conn.createStatement();
                String sql;
                sql = "INSERT into BUYS (id_item, id_buyer, id_seller, photo, item_name, item_price, quantity, consignee, full_address, postal_code, phone_number, cc_number, verification_code, date, time) "
                        + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement dbStatement = conn.prepareStatement(sql);
                dbStatement.setInt(1, id_item);
                dbStatement.setInt(2, id_buyer);
                dbStatement.setInt(3, id_seller);
                dbStatement.setString(4, photo);
                dbStatement.setString(5, item_name);
                dbStatement.setInt(6, item_price);
                dbStatement.setInt(7, quantity);
                dbStatement.setString(8, consignee);
                dbStatement.setString(9, full_address);
                dbStatement.setString(10, postal_code);
                dbStatement.setString(11, phone_number);
                dbStatement.setString(12, cc_number);
                dbStatement.setString(13, verification_code);
                dbStatement.setString(14, date);
                dbStatement.setString(15, time);
                /* Get every data returned by SQL query */
                int rs = dbStatement.executeUpdate();
                stmt.close();
                conn.close();
                result = 1;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return account.getToken();
    }
}
