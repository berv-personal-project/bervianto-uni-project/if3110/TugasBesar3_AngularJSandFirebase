/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author raudi
 */
@WebServlet(urlPatterns = {"/SendMessage"})
public class SendMessage extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            //String USER_AGENT = "Mozilla/5.0";
        
            String url = "https://fcm.googleapis.com/fcm/send";
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException ex) {
            }
            HttpURLConnection con = null;
            con = (HttpURLConnection) obj.openConnection();

            try {
                //add reuqest header
                con.setRequestMethod("POST");
            } catch (ProtocolException ex) {

            }
            //con.setRequestProperty("User-Agent", USER_AGENT);
            //con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "key=AIzaSyAv3-DgWc26htjoVNkGQlzyefujuVf7ZFg");

            //String urlParameters = "{\"data\": { \"score\":\"5\" }, \"to\":\"fuAkBTIw-a0:APA91bGPo_xR3CmQEBrTOSUXf-iJl8BJzLP7THvbAJv5Lf8mtR4PkaBxI5RWiIcn1Kkb1tS0kyo4nLqIQzzzRD9T683bLMDvKRZhx-6iqNO8bzbBjqEz8tmcsdp3APGcPLZNOMznwarB\"}";
            JSONObject data   = new JSONObject();
            //JSONObject to   = new JSONObject();
            JSONObject parent = new JSONObject();

            data.put("title",request.getParameter("sender"));
            data.put("body", request.getParameter("body"));
            
            //to.put("to", "fuAkBTIw-a0:APA91bGPo_xR3CmQEBrTOSUXf-iJl8BJzLP7THvbAJv5Lf8mtR4PkaBxI5RWiIcn1Kkb1tS0kyo4nLqIQzzzRD9T683bLMDvKRZhx-6iqNO8bzbBjqEz8tmcsdp3APGcPLZNOMznwarB");
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(TokenChat.class.getName()).log(Level.SEVERE, null, ex);
            }
            try
            (   Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Chat?zeroDateTimeBehavior=convertToNull",
                    "root",
                    ""); 
                Statement stmt = conn.createStatement()) {
                String sql;
                sql = "SELECT tokenChat FROM tokenChat WHERE username = ?";
                PreparedStatement dbStatement = conn.prepareStatement(sql);
                dbStatement.setString(1, request.getParameter("recipient"));
                JSONObject json = new JSONObject();
                /* Get every data returned by SQL query */
                ResultSet rs = dbStatement.executeQuery();
                if (rs.next()) {
                    parent.put("to", rs.getString("tokenChat"));
                    parent.put("notification", data);
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(SendMessage.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            //out.println(parent.toString());
            con.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(parent.toString());
            wr.flush();

            StringBuilder sb = new StringBuilder();  
            int HttpResult = con.getResponseCode(); 
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;  
                while ((line = br.readLine()) != null) {  
                    sb.append(line + "\n");  
                }
                br.close();
                //out.println("" + sb.toString());  
            } else {
                //out.println(con.getResponseMessage());  
            }  
            
            String method = request.getHeader("Access-Control-Request-Method");
            String header = request.getHeader("Access-Control-Request-Headers");
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Methods", method);
            response.setHeader("Access-Control-Allow-Headers", header);
            
        } catch (JSONException ex) {
            Logger.getLogger(SendMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
