# AngularJS and Firebase for Chat App in SaleStore

## Deskripsi Singkat
AngularJS adalah framework open source berbasis javascript keluaran Google yang biasa digunakan untuk mengatasi masalah-masalah yang ditemukan dalam pengembangan aplikasi dalam satu halaman. AngularJS digunakan dengan menyisipkan tag kedalam atribut HTML dan membuatnya menjadi bagian input atau output dari halaman serta menggunakan ekspresi untuk membuat data menjadi kode HTML.

Firebase Cloud Messaging adalah sebuah cloud service yang biasa digunakan untuk menjadi tempat menyimpan pesan dalam melakukan chatting. FCM digunakan dengan cara mendaftar kedalam layanan tersebut lalu membuat project baru. Dari sini, kita dapat menggunakan FCM untuk menjadi tempat penyimpanan chat dari satu pengguna ke pengguna lainnya.

## Hasil Implementasi
Menggunakan dua account yaitu faiq dan raudi untuk mencoba fitur chatting dari aplikasi web ini. Dibawah ini adalah hasil screenshot dari percobaan chatting:
![](img/screenshot_1.png)
Gambar 1 : Akun raudi masuk ke halaman catalog
![](img/screenshot_2.png)
Gambar 2 : Akun raudi melakukan chat ke akun faiq
![](img/screenshot_3.png)
Gambar 3 : Akun faiq berada pada halaman catalog
![](img/screenshot_4.png)
Gambar 4 : Akun faiq menerima chat dari akun raudi
![](img/screenshot_5.png)
Gambar 5 : Akun faiq melakukan balasan chat
![](img/screenshot_6.png)
Gambar 6 : Tampilan chat pada kedua akun

## Pembagian Tugas

### Chat App Frontend

1. AJAX Request : 13514087
2. Komunikasi controller dan view : 13514081
3. Show-hide komponen : 13514047
4. Data binding : 13514087
5. Controllers : 13514087

### Chat App Backend

1. Fitur Logout : 13514081
2. Send Message : 13514087
3. Token Chat : 13514047


### Fitur security (IP, User-agent)

1. Identity-Service : 13514081
2. MarketPlace : 13514087
3. WebApp : 13514047


## About

Kelompok iDuar :

1. Bervianto Leo P - 13514047
2. Ahmad Faiq Rahman - 13514081
3. Praditya Raudi A - 13514087
