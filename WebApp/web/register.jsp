<%-- 
    Document   : register
    Created on : Nov 6, 2016, 3:09:09 PM
    Author     : raudi
--%>

<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="javax.xml.ws.ProtocolException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookiecek = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookiecek = cookies[i];
            if((cookiecek.getName( )).compareTo("token") == 0 ){
               token = cookiecek.getValue();
               //cookie.setMaxAge(0);
            }
        }
	String userAgent = request.getHeader("User-Agent");
        String test = userAgent;
        String  user = userAgent.toLowerCase();

        String os = "";
        String browser = "";



        //=================OS=======================
         if (userAgent.toLowerCase().indexOf("windows") >= 0 )
         {
             os = "Windows";
         } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
         {
             os = "Mac";
         } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
         {
             os = "Unix";
         } else if(userAgent.toLowerCase().indexOf("android") >= 0)
         {
             os = "Android";
         } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
         {
             os = "IPhone";
         }else{
             os = "UnKnown, More-Info: "+userAgent;
         }
         //===============Browser===========================
        if (user.contains("msie"))
        {
            String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if ( user.contains("opr") || user.contains("opera"))
        {
            if(user.contains("opera"))
                browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if(user.contains("opr"))
                browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
        } else if (user.contains("chrome"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
        {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape-?";

        } else if (user.contains("firefox"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if(user.contains("rv"))
        {
            browser="IE";
        } else
        {
            browser = "UnKnown, More-Info: "+userAgent;
        }
        String ip_address = request.getRemoteAddr();
        account.AccountObject resultValid = portValid.validate(token,browser,ip_address);
	if (resultValid.getValidate() == 1) {
            response.sendRedirect("http://localhost:8000/WebApp/catalog.jsp");
        }
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        String fullname = request.getParameter("fullname");
        String address = request.getParameter("address");
        String postalcode = request.getParameter("postalcode");
        String phonenumber = request.getParameter("phonenumber");
        //String USER_AGENT = "Mozilla/5.0";

        String url = "http://localhost:8001/IdentityService/Register";
        URL obj;
        obj = new URL(url);

        HttpURLConnection con;
        con = (HttpURLConnection) obj.openConnection();
        //add reuqest header
        con.setRequestMethod("POST");

        con.setRequestProperty("User-Agent", browser);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "fullname="+fullname+"&password="+password+"&email="+email
                +"&username="+username+"&address="+address+"&postalcode="+postalcode+"&phonenumber="+phonenumber+"&ip_address="+ip_address;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }

        StringBuffer responsebuff = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    responsebuff.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }

        JSONObject JSobjek = null;  
        JSobjek = new JSONObject(responsebuff.toString());

        //str = (String)JSobjek.get("id");
        int respId = (int)JSobjek.getInt("id");
        //if (!str.equals("null")) {
        //respId = Integer.parseInt(str);
        //}
        if (respId == 0) {
            //response.sendRedirect("http://localhost:8000/WebApp/index.jsp");
        } else {
            Cookie cookie = new Cookie("token",(String)JSobjek.getString("token"));
            response.addCookie(cookie);       
            String tokenChat = request.getParameter("tokenChat");
            response.sendRedirect("http://localhost:8000/WebApp/catalog.jsp?tokenChat=" + tokenChat);
        }
    %>
    <script src="https://www.gstatic.com/firebasejs/3.6.1/firebase.js"></script>
    <script>
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyDwXXDho5ogG5dqqouMLC2tWe_0xkfN9tw",
        authDomain: "test-2c579.firebaseapp.com",
        databaseURL: "https://test-2c579.firebaseio.com",
        storageBucket: "test-2c579.appspot.com",
        messagingSenderId: "222886746942"
      };
      firebase.initializeApp(config);

      const messaging = firebase.messaging();
      messaging.requestPermission()
              .then(function() {
                  console.log('Have permission');
                  return messaging.getToken();
      })
              .then(function(token) {
                  console.log(token);
                  document.getElementById("tokenChat").value=token;
      })
              .catch(function(err) {
                  console.log('Error occured : ');
      })

      messaging.onMessage(function(payload) {
          console.log('onMessage: ', payload)
      })
    </script>
    <head>
	<title>Register</title>
	<link rel="stylesheet" type="text/css" href="css/main.css"> 
	<script type="text/javascript" src="js/validate.js"></script>
    </head>

    <body>
        <h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
        <h2> Please register </h2>
	<div class="notif">
        </div>
	<br>
	<form id="register" method="POST" action="register.jsp">
            <input type="hidden" id="tokenChat" name="tokenChat"/>
            <label for='fullname'> Full Name </label> <br>
            <input type="text" id="fullname" name="fullname"> <br>
            <div id="notif-fullname" class="notif">
            </div>
            <label for='username'> Username </label> <br>
            <input type="text" id="username" name="username"> <br>
            <div id="notif-username" class="notif">
            </div>
            <label for='email'> Email </label> <br>
            <input type="text" id="email" name="email" placeholder="example@example.com"> <br>
            <div id="notif-email" class="notif">
            </div>
            <label for='password'> Password </label><br> <input type="password" id="password" name="password"> <br>
            <div id="notif-password" class="notif">
            </div>
            <label for='confirmpassword'> Confirm Password </label><br> <input type="password" id="confirmpassword" name="confirmpassword"> <br>
            <div id="notif-confirmpassword" class="notif">
            </div>
            <label for='address'> Full Address </label><br>
            <textarea form="register" id="address" name="address"></textarea><br>
            <div id="notif-address" class="notif">
            </div>
            <label for='postalcode'> Postal Code </label><br> <input type="text" id="postalcode" name="postalcode"> <br>
            <div id="notif-postalcode" class="notif">
            </div>			
            <label for='phonenumber'> Phone Number <br> </label><input type="text" id="phonenumber" name="phonenumber"> <br>
            <div id="notif-phonenumber" class="notif">
            </div>
            <div class="submit-style">
        	<input id="button" type="submit" name="register" value="REGISTER">
            </div>
	</form>
	<br>
		
	<div class="register"> <b> Already registered? Login <a href="index.jsp"> here</a> </b></div>
    </body>
</html>
