<%-- 
    Document   : logout
    Created on : Nov 12, 2016, 11:20:00 PM
    Author     : raudi
--%>

<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="javax.xml.ws.ProtocolException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <% 
        
        String url2 = "http://localhost:8003/ChatService/Logout";
        URL obj2 = null;
        try {
            obj2 = new URL(url2);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con2 = null;
        try {
            con2 = (HttpURLConnection) obj2.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con2.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        con2.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters2 = "username="+request.getParameter("username");
        // Send post request
        con2.setDoOutput(true);
        DataOutputStream wr2 = null;
        
        try {
            wr2 = new DataOutputStream(con2.getOutputStream());
            wr2.writeBytes(urlParameters2);
            wr2.flush();
            wr2.close();
        } catch (IOException ex) {
        }
        
        StringBuffer responsebuff2 = new StringBuffer();
        try {
            int responseCode2 = con2.getResponseCode();
            BufferedReader in2 = new BufferedReader(
                new InputStreamReader(con2.getInputStream()));
            String inputLine2;

            while ((inputLine2 = in2.readLine()) != null) {
                    responsebuff2.append(inputLine2);
            }
            in2.close();
        } catch (IOException ex) {
        }
        
        JSONObject JSobjek2 = null;  
        JSobjek2 = new JSONObject(responsebuff2.toString());

        //str = (String)JSobjek.get("id");
        int respId2 = (int)JSobjek2.getInt("result");
        //if (!str.equals("null")) {
        //respId = Integer.parseInt(str);
        //}
        
        String url = "http://localhost:8001/IdentityService/Logout";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }
        
        String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName()).compareTo("token") == 0 ){
               token = cookie.getValue();
               cookie.setMaxAge(0);
            }
        }
        try {
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "token="+token;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer responsebuff = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    responsebuff.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        out.println(responsebuff.toString());
        
        int respId = 0;
        JSONObject JSobjek = null;  
        JSobjek = new JSONObject(responsebuff.toString());
        respId = (int)JSobjek.getInt("result");
        
        if ((respId == 1) && (respId2 == 1)) {
            if (request.getParameter("validate") != null) {
                response.sendRedirect("http://localhost:8000/WebApp/index.jsp?validate="+request.getParameter("validate"));
            } else {
                response.sendRedirect("http://localhost:8000/WebApp/index.jsp");
            }
        }
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logout</title>
    </head>
    <body>
    </body>
</html>
