<%-- 
    Document   : add_product
    Created on : Nov 6, 2016, 3:13:13 PM
    Author     : raudi
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 
<html> 
    <%-- start web service invocation --%>
    <%
	account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName( )).compareTo("token") == 0 ){
               token = cookie.getValue();
               //cookie.setMaxAge(0);
            }
        }
        String userAgent = request.getHeader("User-Agent");
        String test = userAgent;
        String  user = userAgent.toLowerCase();

        String os = "";
        String browser = "";



        //=================OS=======================
         if (userAgent.toLowerCase().indexOf("windows") >= 0 )
         {
             os = "Windows";
         } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
         {
             os = "Mac";
         } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
         {
             os = "Unix";
         } else if(userAgent.toLowerCase().indexOf("android") >= 0)
         {
             os = "Android";
         } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
         {
             os = "IPhone";
         }else{
             os = "UnKnown, More-Info: "+userAgent;
         }
         //===============Browser===========================
        if (user.contains("msie"))
        {
            String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if ( user.contains("opr") || user.contains("opera"))
        {
            if(user.contains("opera"))
                browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if(user.contains("opr"))
                browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
        } else if (user.contains("chrome"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
        {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape-?";

        } else if (user.contains("firefox"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if(user.contains("rv"))
        {
            browser="IE";
        } else
        {
            browser = "UnKnown, More-Info: "+userAgent;
        }
        String ip_address = request.getRemoteAddr();
        account.AccountObject resultValid = portValid.validate(token,browser,ip_address);
        if (resultValid.getValidate() == 1) {   
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",resultValid.getToken());
            response.addCookie(cookieExpires);
        } else {
            response.sendRedirect("http://localhost:8000/WebApp/logout.jsp?username="+resultValid.getUsername()+"&validate="+resultValid.getValidate());
            //out.println(resultValid.getUsername());
        }
    %>
    <head> 
	<title>Add Product</title> 
	<link rel="stylesheet" type="text/css" href="css/main.css">  
	<script type="text/javascript" src="js/validate.js"></script>
    </head> 
	
    <body id="body-color"> 
        <div>
            <h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
	</div>
	<div class="atas">
            Hi, <%out.println(resultValid.getUsername());%>!<br>
            <a href="logout.jsp?username=<%=resultValid.getUsername()%>" class="logout"><font size="1">logout</font></a>
	</div>
	<div class="navbar">
            <ul>
		<li><a href="catalog.jsp">Catalog</a></li>
		<li><a href="your_product.jsp">Your Products</a></li>
		<li><a class="active" href="add_product.jsp">Add Products</a></li>
		<li><a href="sales.jsp">Sales</a></li>
		<li><a href="purchases.jsp">Purchases</a></li>
            </ul>
            <br><br>
	</div>
	<div>
            <h2> Please add your product here </h2>
        </div>
        <%
            Date dNow = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat ("EEEE',' dd MMMM y");
            SimpleDateFormat timeFormat = new SimpleDateFormat ("HH.mm");
         %>
	<form method="post" id="add_product" action="AddProductServlet" enctype="multipart/form-data">
            <input id="accessToken" type="hidden" name="accessToken" value="<%=resultValid.getToken() %>">
            <input id="userAgent" type="hidden" name="userAgent" value="<%= browser %>">
            <input id="ip" type="hidden" name="ip" value="<%= ip_address %>">
            <label for="product_name">Name</label><br>
            <input type="text" name="name" id="name"><br>
            <input type="hidden" name="id" id="id" value="<%=resultValid.getId()%>"><br>
            <div id="notif-name" class="notif">
            </div>
            <label for="product_desc">Description (max 200 chars)</label><br>
            <textarea rows="5" cols="107" name="description" id="description"></textarea><br>
            <div id="notif-description" class="notif">
            </div>
            <label for="product_price">Price (IDR)</label><br>
            <input type="text" name="price" id="price" size="200"><br>
            <div id="notif-price" class="notif">
            </div>
            <label for="product_photo">Photo</label><br>
            <input type="file" accept="image/*" align="middle" id="photo" name="photo"><br>
            <div id="notif-photo" class="notif">
            </div>
            <input type="hidden" id="date" name="date" value="<%=dateFormat.format(dNow) %>">
            <input type="hidden" id="time" name="time" value="<%=timeFormat.format(dNow) %>">
            <div class="submit-style">
                <input id="button" type="submit" name="add_product" value="ADD"> 
                <input id="cancel" class="cancel" type="button" size="20" value="CANCEL" onclick="window.location.href='your_product.jsp'">  
            </div>
	</form>	
    </body> 
</html>
