/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClientServlet;

import item.Items_Service;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author raudi
 */
@WebServlet(name = "DeleteProductServlet", urlPatterns = {"/DeleteProductServlet"})
public class DeleteProductServlet extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8002/MarketplaceWebService/Items.wsdl")
    private Items_Service service;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String accessToken = request.getParameter("accessToken");
            String userAgent = request.getParameter("userAgent");
            String ip = request.getParameter("ip");
            String strId = request.getParameter("id");
            int id = Integer.parseInt(strId);
            
            String newToken = deleteProduct(accessToken, id, userAgent, ip);
            String token = null;
            Cookie cookie = null;
            Cookie[] cookies = null;
            cookies = request.getCookies();
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   //cookie.setMaxAge(0);
                }
            }
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",newToken);
            response.addCookie(cookieExpires);
            response.sendRedirect("your_product.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String deleteProduct(java.lang.String accessToken, int id, java.lang.String userAgent, java.lang.String ip) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        item.Items port = service.getItemsPort();
        return port.deleteProduct(accessToken, id, userAgent, ip);
    }
    

}
